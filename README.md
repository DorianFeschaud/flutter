Pré-requis:
 - Avoir Node 18 et Flutter

Ouvir le projet dans Android Studio

Le bouton download en haut à droite de la top-app-bar pour charger l'ensemble des films
Le bouton + en haut à droite de la top-app-bar pour ajouter un nouveau film (uniquement le titre)
Le bouton - sur chaque film de la liste pour le supprimer
Le bouton edit sur chaque film de la liste pour le modifier (uniquement le titre)
On peut clqiuer sur chaque film pour avoir plus de détails