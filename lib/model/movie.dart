import 'dart:convert';
import 'package:flutter/services.dart';

class Movie {
  Movie({
    required this.Title,
    required this.Year,
    required this.Rated,
    required this.Released,
    required this.Runtime,
    required this.Genre,
    required this.Director,
    required this.Writer,
    required this.Actors,
    required this.Plot,
    required this.Language,
    required this.Country,
    required this.Awards,
    required this.Poster,
    required this.Metascore,
    required this.imdbRating,
    required this.imdbVotes,
    required this.imdbID,
    required this.Type,
    required this.Response,
    required this.Images,
  });
  late final String Title;
  late final String Year;
  late final String Rated;
  late final String Released;
  late final String Runtime;
  late final String Genre;
  late final String Director;
  late final String Writer;
  late final String Actors;
  late final String Plot;
  late final String Language;
  late final String Country;
  late final String Awards;
  late final String Poster;
  late final String Metascore;
  late final String imdbRating;
  late final String imdbVotes;
  late final String imdbID;
  late final String Type;
  late final String Response;
  late final List<String> Images;

  Movie.fromJson(Map<String, dynamic> json){Title = json['Title'];
    Year = json['Year'];
    Rated = json['Rated'];
    Released = json['Released'];
    Runtime = json['Runtime'];
    Genre = json['Genre'];
    Director = json['Director'];
    Writer = json['Writer'];
    Actors = json['Actors'];
    Plot = json['Plot'];
    Language = json['Language'];
    Country = json['Country'];
    Awards = json['Awards'];
    Poster = json['Poster'];
    Metascore = json['Metascore'];
    imdbRating = json['imdbRating'];
    imdbVotes = json['imdbVotes'];
    imdbID = json['imdbID'];
    Type = json['Type'];
    Response = json['Response'];
    Images = List.castFrom<dynamic, String>(json['Images']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['Title'] = Title;
    _data['Year'] = Year;
    _data['Rated'] = Rated;
    _data['Released'] = Released;
    _data['Runtime'] = Runtime;
    _data['Genre'] = Genre;
    _data['Director'] = Director;
    _data['Writer'] = Writer;
    _data['Actors'] = Actors;
    _data['Plot'] = Plot;
    _data['Language'] = Language;
    _data['Country'] = Country;
    _data['Awards'] = Awards;
    _data['Poster'] = Poster;
    _data['Metascore'] = Metascore;
    _data['imdbRating'] = imdbRating;
    _data['imdbVotes'] = imdbVotes;
    _data['imdbID'] = imdbID;
    _data['Type'] = Type;
    _data['Response'] = Response;
    _data['Images'] = Images;
    return _data;
  }
  static Future<String> _loadMoviesAsset() async{
    return await rootBundle.loadString('assets/film.json');
  }

  static Future<List<Movie>> loadMovies() async{
    String jsonString = await _loadMoviesAsset();
    List<dynamic> jsonResponse = jsonDecode(jsonString)['movies'] as List;
    List<Movie> rep=[];
    jsonResponse.forEach((element) {rep.add(Movie.fromJson(element));});
    return rep;
  }

  String toString(){
    return Title;
  }

  static addFilm(title){
    return Movie(Title:title, Year:"1234", Rated: "rated", Released:"2020", Runtime:"123", Genre:"bidon", Director:"The director", Writer:"The writer",
        Actors:"Actors", Plot:"Un super film bidon", Language:"FR", Country:"None",
        Awards:"coucou", Poster:"https://images-na.ssl-images-amazon.com/images/M/MV5BNzM2MDk3MTcyMV5BMl5BanBnXkFtZTcwNjg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
        Metascore: "456", imdbRating: "800", imdbVotes:"123", imdbID: "1234", Type:"movie",Response:"True" , Images:["https://images-na.ssl-images-amazon.com/images/M/MV5BMjEyOTYyMzUxNl5BMl5BanBnXkFtZTcwNTg0MTUzNA@@._V1_SX1500_CR0,0,1500,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BNzM2MDk3MTcyMV5BMl5BanBnXkFtZTcwNjg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY2ODQ3NjMyMl5BMl5BanBnXkFtZTcwODg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMxOTEwNDcxN15BMl5BanBnXkFtZTcwOTg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTYxMDg1Nzk1MV5BMl5BanBnXkFtZTcwMDk0MTUzNA@@._V1_SX1500_CR0,0,1500,999_AL_.jpg"]);
  }

  static editFilm(movie, title){
    return Movie(Title: title, Year: movie.Year, Rated: movie.Rated, Released: movie.Released, Runtime: movie.Runtime, Genre: movie.Genre, Director: movie.Director, Writer: movie.Writer,
        Actors: movie.Actors, Plot: movie.Plot, Language: movie.Language, Country: movie.Country,
        Awards: movie.Awards, Poster: movie.Poster, Metascore: movie.Metascore, imdbRating: movie.imdbRating, imdbVotes: movie.imdbVotes, imdbID: movie.imdbID, Type: movie.Type, Response: movie.Response, Images: movie.Images);
  }



  static getFilmBidon(){
    return Movie(Title:"Film Bidon", Year:"1234", Rated: "rated", Released:"2020", Runtime:"123", Genre:"bidon", Director:"The director", Writer:"The writer",
        Actors:"Actors", Plot:"Un super film bidon", Language:"FR", Country:"None",
        Awards:"coucou", Poster:"https://images-na.ssl-images-amazon.com/images/M/MV5BNzM2MDk3MTcyMV5BMl5BanBnXkFtZTcwNjg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
        Metascore: "456", imdbRating: "800", imdbVotes:"123", imdbID: "1234", Type:"movie",Response:"True" , Images:["https://images-na.ssl-images-amazon.com/images/M/MV5BMjEyOTYyMzUxNl5BMl5BanBnXkFtZTcwNTg0MTUzNA@@._V1_SX1500_CR0,0,1500,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BNzM2MDk3MTcyMV5BMl5BanBnXkFtZTcwNjg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY2ODQ3NjMyMl5BMl5BanBnXkFtZTcwODg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTMxOTEwNDcxN15BMl5BanBnXkFtZTcwOTg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg",
          "https://images-na.ssl-images-amazon.com/images/M/MV5BMTYxMDg1Nzk1MV5BMl5BanBnXkFtZTcwMDk0MTUzNA@@._V1_SX1500_CR0,0,1500,999_AL_.jpg"]);
  }

}