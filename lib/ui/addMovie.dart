import 'package:flutter/material.dart';
import '/model/movie.dart';

class AddMovie extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text("Add a movie"),
      ),

      body: Center(
        child: TextField(
          decoration: InputDecoration(
            labelText: "title",
          ),
          onSubmitted: (String value) {
            Navigator.pop(context,Movie.addFilm(value));
              },
            ),
        )
      );
  }

}
