import 'package:flutter/material.dart';
import '/model/movie.dart';

class EditMovie extends StatefulWidget {
  late Movie movie;

  EditMovie(this.movie, {super.key});


  @override
  State<EditMovie> createState() => _EditMovie(movie);
}

class _EditMovie extends State<EditMovie> {

  late Movie movie;

  _EditMovie(this.movie);


  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          title: Text("Edit a movie"),
        ),

        body: Center(
          child: TextField(
            decoration: InputDecoration(
              labelText: "title",
            ),
            onSubmitted: (String value) {
              Navigator.pop(context,Movie.editFilm(movie, value));
            },
          ),
        )
    );
  }
}